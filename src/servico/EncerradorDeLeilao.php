<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 05/12/2017
 * Time: 14:42
 */

namespace src\servico;

use PHPUnit\Framework\MockObject\RuntimeException;
use src\dominio\Leilao;
use src\dao\LeilaoDao;


class EncerradorDeLeilao
{
    private $total = 0;

    private $dao;

    public function __construct(LeilaoDao $dao ) {
        $this->dao = $dao;
    }

    public function finalizaLeiloes(){
        $leiloesAbertos = $this->dao->corrente();
        if( count( $leiloesAbertos ) == 0 ) {
            throw new RuntimeException('Não existem leiloes para finalizar');
        }

        foreach( $leiloesAbertos as $leilao ) {
            if ($this->comecouSemanaPassada($leilao)) {
                $leilao->encerraLeilao();
                $this->total++;
                $this->dao->update($leilao);
            }
        }
    }

    private function comecouSemanaPassada(Leilao $leilao) {
        return $this->diasEntre($leilao->getDataAbertura(), new \DateTime()) > 6;
    }

    private function diasEntre( \DateTime $inicio, \DateTime $fim ) {
        $date = clone $inicio;
        $diasNoIntervalo = 0;
        while($date < $fim ) {
            $date->add(new \DateInterval('P1D'));
            $diasNoIntervalo ++;
        }
        return $diasNoIntervalo;
    }


    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     * @return EncerradorDeLeilao
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDao()
    {
        return $this->dao;
    }

    /**
     * @param mixed $dao
     * @return EncerradorDeLeilao
     */
    public function setDao($dao)
    {
        $this->dao = $dao;
        return $this;
    }


}