<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 04/12/2017
 * Time: 15:37
 */

namespace src\servico;


class FiltroDeLances
{
    public function filtrar( array $lances ) {
        $resultado = [];
        foreach( $lances as $lance ) {
            $valor = $lance->getValor();

            if ($valor > 5000) {
                $resultado[] = $lance;
            } else if ($valor > 1000 && $valor < 3000) {
                $resultado[] = $lance;
            } else if ($valor > 500 && $valor < 700) {
                $resultado[] = $lance;
            }
        }
        return $resultado;
    }
}