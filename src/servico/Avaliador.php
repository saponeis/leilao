<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 04/12/2017
 * Time: 10:01
 */

namespace src\servico;


use PHPUnit\Framework\MockObject\RuntimeException;
use src\dominio\Leilao;

class Avaliador
{

    private $menorValor = INF ;
    private $maiorValor = -INF;
    private $leilao;
    private $valorMedio;
    private $maiores;

    public function __construct(Leilao $leilao)
    {
        $this->leilao = $leilao;
    }


    public function avalia() {

        $valor = 0;
        $lances = $this->leilao->getLances();
        if( empty( $this->leilao->getLances() ) ) {
            throw new RuntimeException('Leilão sem Lances');
        } else {
            $valores = [];
            foreach( $this->leilao->getLances() as $lance ) {
                $valores[] = $lance->getValor();

                $valor += $lance->getValor();

                if ( $lance->getValor() < $this->menorValor ) {
                    $this->menorValor = $lance->getValor();
                }

                if ( $lance->getValor() > $this->maiorValor ) {
                    $this->maiorValor = $lance->getValor();
                }
                $this->valorMedio = $valor/count( $this->leilao->getLances() );
            }
            arsort($valores);
            $this->maiores = array_slice( $valores, 0,3);


        }
    }


    /**
     * @return mixed
     */
    public function getMenorValor()
    {
        return $this->menorValor;
    }

    /**
     * @param mixed $menorValor
     */
    public function setMenorValor($menorValor)
    {
        $this->menorValor = $menorValor;
    }

    /**
     * @return int
     */
    public function getMaiorValor()
    {
        return $this->maiorValor;
    }

    /**
     * @param int $maiorValor
     */
    public function setMaiorValor($maiorValor)
    {
        $this->maiorValor = $maiorValor;
    }

    /**
     * @return mixed
     */
    public function getLeilao()
    {
        return $this->leilao;
    }

    /**
     * @param mixed $leilao
     */
    public function setLeilao($leilao)
    {
        $this->leilao = $leilao;
    }

    /**
     * @return mixed
     */
    public function getValorMedio()
    {
        return $this->valorMedio;
    }

    /**
     * @param mixed $valorMedio
     */
    public function setValorMedio($valorMedio)
    {
        $this->valorMedio = $valorMedio;
    }
    /**
     * @return mixed
     */
    public function getMaiores()
    {
        return $this->maiores;
    }

    /**
     * @param mixed $maiores
     * @return Avaliador
     */
    public function setMaiores($maiores)
    {
        $this->maiores = $maiores;
        return $this;
    }



}