<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 05/12/2017
 * Time: 15:48
 */

namespace src\dao;

interface LeilaoDao
{
    public function corrente();
    public function update();

}