<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 04/12/2017
 * Time: 09:17
 */

namespace src\dominio;

class Leilao {

    private $lances = [];
    private $descricao;
    private $valorInicial;
    private $status;
    private $dataAbertura;

    /**
     * @return mixed
     */

    public function __construct( $descricao){
        $this->setDescricao($descricao);
        $this->dataAbertura = new \DateTime();
        $this->status = 'Criado';
    }

    public function propoe(Lance $lance){


        if(count($this->lances) != 0) {

            if (!$this->verificaSeMenorQueAnterior($lance)) {
                return false;
            }

            if (!$this->verificaQuantidadePor($lance->getUsuario())) {
                return false;
            }

            if ($this->verificaSeUsuarioDeuUltimoLance($lance->getUsuario())) {
                throw new \RuntimeException('Usuario deu 2 lances seguidos');
            }
        }

        $this->lances[] = $lance;
        return true;
    }

    public function encerraLeilao(){
        $this->status = "Encerrado";
    }

    private function verificaSeUsuarioDeuUltimoLance($usuario){
        $ultimoLance = $this->getUltimoLance();
        return ($ultimoLance->getUsuario() == $usuario);

        }
    private function verificaSeMenorQueAnterior($lance){

        $ultimoLance = $this->getUltimoLance();
        if(($ultimoLance)->getValor() < $lance->getValor()) {
            return true;
        }
        return false;
    }

    public function isEncerrado(){
        return $this->status == 'Encerrado';
    }

    public function getUltimoLance() {
        return end($this->lances);
    }


    private function verificaQuantidadePor(Usuario $usuario) {
        $qdt = 0;
        foreach($this->getLances() as $lance) {
            if ($lance->getUsuario() == $usuario) $qdt++;
        }
        return $qdt < 5;

    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     * @return Leilao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValorInicial()
    {
        return $this->valorInicial;
    }

    /**
     * @param mixed $valorInicial
     * @return Leilao
     */
    public function setValorInicial($valorInicial)
    {
        $this->valorInicial = $valorInicial;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return Leilao
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataAbertura()
    {
        return $this->dataAbertura;
    }

    /**
     * @param mixed $dataAbertura
     * @return Leilao
     */
    public function setDataAbertura($dataAbertura)
    {
        $this->dataAbertura = $dataAbertura;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLances()
    {
        return $this->lances;
    }

    /**
     * @param mixed $lances
     * @return Leilao
     */
    public function setLances($lances)
    {
        $this->lances = $lances;
        return $this;
    }




}