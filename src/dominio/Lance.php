<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 04/12/2017
 * Time: 09:36
 */

namespace src\dominio;


class Lance {
    private $valor;
    private $usuario;

    public function __construct( $valor, Usuario $usuario )
    {
        $this->valor = $valor;
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     * @return Lance
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     * @return Lance
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }



}