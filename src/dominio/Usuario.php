<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 04/12/2017
 * Time: 09:42
 */

namespace src\dominio;

class Usuario
{
    private $id;
    private $nome;

    public function __construct($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return usuario
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     * @return usuario
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

}