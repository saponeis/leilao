<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 05/12/2017
 * Time: 11:22
 */

namespace test\builder;

use src\dominio\Lance;
use src\dominio\Usuario;
use src\dominio\Leilao;

require_once '../../vendor/autoload.php';

class LeilaoBuilder
{

    public function __construct()
    {
        $this->leilao = new Leilao('');
    }

    public function comLance($valor, Usuario $usuario) {
        $this->leilao->propoe(new Lance($valor, $usuario));
        return $this;
    }

    public function comDescricao($descricao) {
        $this->leilao->setDescricao($descricao);
        return $this;
    }

    public function naData($data) {
        $this->leilao->setDataAbertura($data);
        return $this;
    }

    public function criar(){
        return $this->leilao;
    }


}