<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 05/12/2017
 * Time: 15:36
 */

namespace test\servico;

require_once '../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use src\servico\EncerradorDeLeilao;
use src\dao\LeilaoDao;
use test\builder\LeilaoBuilder;


class EncerradorDeLeilaoTest extends TestCase
{

    public function testDeveEncerrarLeilaoComMaisDeUmaSemana() {
        $antiga = new \DateTime('1940-01-01');

        $builder = new LeilaoBuilder();
        $leilao = $builder->comDescricao('Teste Com Mock')->naData($antiga)->criar();

        $mock = $this->createMock(LeilaoDao::class);
        $mock->method("corrente")
            ->will($this->returnValue([$leilao]));

        $encerrador = new EncerradorDeLeilao($mock);

        $encerrador->finalizaLeiloes();

        $this->assertTrue($leilao->isEncerrado());

    }

    public function testDeveManterAbertoLeilaoComMenosDeUmaSemana() {
        $date = new \DateTime();

        $builder = new LeilaoBuilder();
        $leilao = $builder->comDescricao('Teste Com Mock')->naData($date)->criar();

        $mock = $this->createMock(LeilaoDao::class);
        $mock->method("corrente")
            ->will($this->returnValue([$leilao]));

        $encerrador = new EncerradorDeLeilao($mock);

        $encerrador->finalizaLeiloes();

        $this->assertFalse($leilao->isEncerrado());

    }

    /**
     * @expectedException \RuntimeException
     */
    public function testEncerradorSemLeilao() {
        $mock = $this->createMock(LeilaoDao::class);
        $mock->method("corrente")
            ->will($this->returnValue([]));

        $encerrador = new EncerradorDeLeilao($mock);

        $encerrador->finalizaLeiloes();

        $this->assertEquals($encerrador->getTotal(), 0);
    }

}