<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 05/12/2017
 * Time: 14:19
 */

namespace test\servico;


use PHPUnit\Framework\TestCase;
use src\dominio\Usuario;
use test\builder\LeilaoBuilder;
use src\servico\FiltroDeLances;

class FiltroDeLancesTest extends TestCase
{

    private $usuario1;
    private $usuario2;
    private $leilaoBuilder;


    public function testLancesEntre1000e3000() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(400, $this->usuario1)
            ->comLance(500, $this->usuario2)
            ->comLance(1000, $this->usuario1)
            ->comLance(2000, $this->usuario2)
            ->comLance(3000, $this->usuario1)
            ->criar();

        $filtro = new FiltroDeLances();
        $resultado = $filtro->filtrar($leilao->getLances());

        $this->assertEquals(1, count($resultado));
        $this->assertEquals(2000, $resultado[0]->getValor());
    }

    public function testLancesEntre500e700() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(500, $this->usuario1)
            ->comLance(600, $this->usuario2)
            ->comLance(700, $this->usuario1)
            ->comLance(800, $this->usuario2)
            ->comLance(900, $this->usuario1)
            ->criar();

        $filtro = new FiltroDeLances();
        $resultado = $filtro->filtrar($leilao->getLances());

        $this->assertEquals(1, count($resultado));
        $this->assertEquals(600, $resultado[0]->getValor());
    }

    public function testLancesMaiorQue5000() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->comLance(400, $this->usuario2)
            ->comLance(500, $this->usuario1)
            ->comLance(5000, $this->usuario2)
            ->comLance(5001, $this->usuario1)
            ->criar();

        $filtro = new FiltroDeLances();
        $resultado = $filtro->filtrar($leilao->getLances());

        $this->assertEquals(1, count($resultado));
        $this->assertEquals(5001, $resultado[0]->getValor());
    }


    public function testLancesForaDoFiltro() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->comLance(400, $this->usuario2)
            ->comLance(500, $this->usuario1)
            ->comLance(5000, $this->usuario2)
            ->criar();

        $filtro = new FiltroDeLances();
        $resultado = $filtro->filtrar($leilao->getLances());

        $this->assertEquals(0, count($resultado));
    }

    public function setup() {
        $this->usuario1 = New Usuario('SapoNeis');
        $this->usuario2 = New Usuario('Nanner');
        $this->leilaoBuilder = New LeilaoBuilder();

    }


}