<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 05/12/2017
 * Time: 14:14
 */

namespace test\servico;


require_once '../../vendor/autoload.php';

use PHPUnit\Framework\MockObject\RuntimeException;
use PHPUnit\Framework\TestCase;
use src\dominio\Usuario;
use src\servico\Avaliador;

use test\builder\LeilaoBuilder;


class AvaliadorTest  extends TestCase
{

    private $usuario1;
    private $usuario2;
    private $leilaoBuilder;


    public function testOrdemCrescente() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->comLance(200, $this->usuario2)
            ->comLance(300, $this->usuario1)
            ->comLance(400, $this->usuario2)
            ->criar();


        $avaliador = new Avaliador($leilao);
        $avaliador->avalia();

        $menorEsperado = 100;
        $maiorEsperado = 400;

        $this->assertEquals($maiorEsperado, $avaliador->getMaiorValor());
        $this->assertEquals($menorEsperado, $avaliador->getMenorValor());

    }

    public function testUnicoLance() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->criar();

        $avaliador = new Avaliador($leilao);

        $avaliador->avalia();

        $maiorEsperado = 100;
        $menorEsperado = 100;

        $this->assertEquals($maiorEsperado, $avaliador->getMaiorValor());
        $this->assertEquals($menorEsperado, $avaliador->getMenorValor());
    }

    /**
     * @expectedException RuntimeException
     */
    public function testSemLance() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->criar();

        $avaliador = new Avaliador($leilao);

        $avaliador->avalia();

        $maiorEsperado = 0;
        $menorEsperado = 0;
        $valorMedio = 0;

        $this->assertEquals($valorMedio, $avaliador->getValorMedio());
        $this->assertEquals($maiorEsperado, $avaliador->getMaiorValor());
        $this->assertEquals($menorEsperado, $avaliador->getMenorValor());
    }



    public function testValorMedio() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(20, $this->usuario1)
            ->comLance(40, $this->usuario2)
            ->criar();

        $avaliador = new Avaliador($leilao);
        $avaliador->avalia();
        $valorMedio = 30;

        $this->assertEquals($valorMedio, $avaliador->getValorMedio());

    }

    public function testTresMaiores() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->comLance(200, $this->usuario2)
            ->comLance(300, $this->usuario1)
            ->comLance(400, $this->usuario2)
            ->criar();

        $avaliador = new Avaliador($leilao);

        $avaliador->avalia();

        $tresMaiores = [400,300,200];


        $this->assertEquals($tresMaiores, $avaliador->getMaiores());

    }

    public function setup()
    {
        $this->usuario1 = New Usuario('SapoNeis');
        $this->usuario2 = New Usuario('Nanner');
        $this->leilaoBuilder = New LeilaoBuilder();

    }


}
