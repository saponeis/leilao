<?php
/**
 * Created by PhpStorm.
 * User: sapon
 * Date: 05/12/2017
 * Time: 09:06
 */

namespace test\dominio;

require_once '../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

use src\dominio\Usuario;
use test\builder\LeilaoBuilder;

class LeilaoTest extends TestCase
{

    private $usuario1;
    private $usuario2;
    private $leilaoBuilder;

    /**
     *  @beforeClass
     **/
    public static function comeca() {
        echo 'Iniciando os Testes' . PHP_EOL;
    }

    public function testLeilaoComUmLance() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->criar();

        $esperado = 1;

        $this->assertEquals($esperado, count($leilao->getLances()));

    }

    public function testLanceComMenorValorQueOAnterior() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->comLance(50, $this->usuario2)
            ->comLance(120, $this->usuario2)
            ->criar();

        $esperado = 2;

        $this->assertEquals($esperado, count($leilao->getLances()));

    }

    /**
     * @expectedException RuntimeException
     */
    public function testDoisLancesSeguidosDoMesmoUsuario() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->comLance(150, $this->usuario1)
            ->criar();

        $esperado = 1;

        $this->assertEquals($esperado, count($leilao->getLances()));
    }



    public function testLeilaoComVariosLances() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->comLance(150, $this->usuario2)
            ->comLance(200, $this->usuario1)
            ->criar();

        $esperado = 3;

        $this->assertEquals($esperado, count($leilao->getLances()));

    }

    public function testNaoDeveAceitarMaisDeCintoLancesPorUsuario() {

        $leilao = $this->leilaoBuilder->comDescricao('Conta Premium')
            ->comLance(100, $this->usuario1)
            ->comLance(150, $this->usuario2)
            ->comLance(200, $this->usuario1)
            ->comLance(250, $this->usuario2)
            ->comLance(300, $this->usuario1)
            ->comLance(350, $this->usuario2)
            ->comLance(400, $this->usuario1)
            ->comLance(450, $this->usuario2)
            ->comLance(500, $this->usuario1)
            ->comLance(550, $this->usuario2)
            ->comLance(600, $this->usuario1)
            ->criar();

        $esperado= 10;
        $this->assertEquals($esperado, count($leilao->getLances()));

    }

    public function setup()
    {
        $this->usuario1 = New Usuario('SapoNeis');
        $this->usuario2 = New Usuario('Nanner');
        $this->leilaoBuilder = New LeilaoBuilder();

    }
}